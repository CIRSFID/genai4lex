# Scenario #A6: Prototipo di chatbot “Chat-EUR-Lex” basato su Retrieval-Augmented Generation (RAG), per interrogare le norme europee (EUR-Lex) in italiano e in inglese.

**Obiettivo** 
Facilitare l’interrogazione (ricerca, riassunto, traduzione) di documenti legislativi europei in italiano e inglese (pubblicati in EUR-Lex) mediante un chatbot basato su LLM, limitando le cosiddette “allucinazioni”. Il sistema può essere applicato alla normativa italiana o ad altri dataset (regolamenti, disegni di legge, ecc).

**Descrizione**
Il chatbot permette all’utente di formulare domande e richieste (ad es. “riassumi”, “traduci”, ecc) tramite un’interfaccia in stile “chatGPT”. Il sistema effettua una ricerca semantica nei dataset normativi e genera la risposta basandosi solamente sulle porzioni di testo di leggi e regolamenti rilevanti, per evitare le allucinazioni e fornire il riferimento alla fonte.  

**Soluzione tecnica**
Il prototipo implementa un sistema di Retrieval-Augmented Generation (RAG), cioè un motore di ricerca semantico integrato con un sistema di IA generativa (GenLLM). Come GenLLM, nella versione corrente del prototipo viene utilizzato GPT-4 di OpenAI, che fino ad aprile 2024 risultava il migliore nella generazione delle risposte, ma il sistema può utilizzare altri LLM, open source o commerciali. 

**Funzionamento** 
L’utente accede semplicemente ad un’interfaccia web in stile ChatGPT e scrive le domande o richieste. Il sistema genera la risposta e mostra le porzioni di leggi e regolamenti recuperati tramite il motore di ricerca semantico e utilizzati per generare la risposta.

**Esempio d’uso**
L’utente scrive la richiesta: “Trova la norma europea, ancora in vigore, che prevede l'istituzione del Gruppo europeo sull’etica nelle scienze e nelle nuove tecnologie (GEE)”. Il chatbot genera la risposta e mostra nella colonna di destra le porzioni di leggi e regolamenti utilizzati per generare la risposta.

**Link alla demo e alla documentazione**
Demo accessibile su richiesta (la demo non è pubblica): https://huggingface.co/spaces/AptusAI/Chat-EUR-Lex 
Documentazione del progetto e del prototipo Chat-EUR-Lex: https://github.com/Aptus-AI/chat-eur-lex/ 
Pubblicazione scientifica al convegno Ital-IA 2024: "Improving the accessibility of EU laws: the Chat-EUR-Lex project" [PDF]
Manola Cherubini, Francesco Romano, Andrea Bolioli, Lorenzo De Mattei, Mattia Sangermano (IGSG-CNR, Aptus.AI)