**Scenario #B2** <br>
Prototipo di sistema di analisi emendamenti.

**Obiettivo** <br>
Facilitare e velocizzare l'analisi degli emendamenti, navigando i riferimenti normativi citati su banche dati pubbliche.

**Descrizione** <br> 
Il sistema di analisi qui proposto permette all’utente di scrivere il testo di un emendamento e l'articolo del provvedimento cui l'emendamento si applica tramite un’interfaccia intuitiva.

**Soluzione tecnica** <br> 
Abbiamo esposto una API a tale sistema di analisi. Esso usa un pipeline di prompt engineering di IA generativa (LLMs) coadiuvata da componenti di IA classificativa (RoBerta) ed espressioni regolari per identificare e navigare la normativa pertinente alla comprensione dell'effetto giuridico del testo, e la usa per spiegare in un titolo didascalico e una sintesi più discorsiva di tale effetto. Il sistema assegna, inoltre, una serie di parole chiave abbastanza generali, recuperate dalle competenze delle Commissioni della Camera dei Deputati (https://www.camera.it/leg18/737).

**Funzionamento** 
1. Navigare al link https://asimov.law/camera.html
2. Inserire la propria chiave API (nota in fondo)
3. Inserire nei due appositi campi rispettivamente il testo di un emendamento e dell'articolo di riferimento, o scegliere uno degli esempi dalla barra laterale
4. Clicca su Analizza e attendi il completamento dell'analisi

**Esempio d’uso** <br> La pagina propone 5 esempi d'uso già pronti, contenenti ciascuno un emendamento a un determinato atto in esame della Camera dei Deputati.

**Risorse** <br>
Link alla demo https://asimov.law/camera.html <br>
Chiave API *ak-XtJovY34Ib54rsAXldnJpC1pTzniO3OZ* <br>
Screenshot di esempio https://asimov.law/screenshot_asimov.png
