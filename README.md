# GenAI4Lex

Questa è la documentazione, il codice, i documenti del gruppo di lavoro GenAILex che ha sottomesso due progetti in risposta  alla manifestazione di interesse per la raccolta di proposte per l’utilizzo dell’intelligenza artificiale generativa per la Camera dei Deputati.

**Table of content:**

 - [A. REPERIRE E ORGANIZZARE INFORMAZIONI QUALIFICATE: definizione di strumenti (proof of concept) a supporto delle attività di predisposizione della documentazione per i deputati.](#areperire-e-organizzare-informazioni-qualificate-definizione-di-strumenti-proof-of-concept-a-supporto-delle-attivit%C3%A0-di-predisposizione-della-documentazione-per-i-deputati)

 - [B. SUPPORTARE LA PREDISPOSIZIONE DEGLI ATTI DI INIZIATIVA: definizione di strumenti (proof of concept) a supporto della predisposizione di proposte di legge e di atti di indirizzo e di controllo.](#bsupportare-la-predisposizione-degli-atti-di-iniziativa-definizione-di-strumenti-proof-of-concept-a-supporto-della-predisposizione-di-proposte-di-legge-e-di-atti-di-indirizzo-e-di-controllo)


<a id="call-a"/>

## A.	REPERIRE E ORGANIZZARE INFORMAZIONI QUALIFICATE: definizione di strumenti (proof of concept) a supporto delle attività di predisposizione della documentazione per i deputati.
### Obiettivo: 
Reperire informazioni qualificate durante l’istruttoria di una proposta di legge è un lavoro difficoltoso e impegnativo. Se poi occorre anche incrociare le molte banche dati disponibili in un’ottica comparata risulta particolarmente ostico. Il progetto che si propone offre supporto alle attività di predisposizione della documentazione per l’istruttoria.
Target: E’ rivolto ai deputati, alle loro segreterie, ai loro collaboratori legislativi che devono preliminarmente svolgere attività di studio, istruttoria, analisi giuridica. Sarà utile anche per gli uffici della Camera dei Deputati poter accedere rapidamente alle informazioni e poterle confrontare.
 
### Strumenti:
1.	COLLEGARE: #A1. Rispetto ad una tematica inserita dall’utente si chiede al sistema di estrarre i materiali rilevanti utilizzando banche dati eterogenee multilingue (inglese e italiano) quali la legislazione europea (Eur-Lex) e quella nazionale (Normattiva). Si integra in questa ricerca anche la consultazione delle decisioni della Corte Costituzionale (UNIBO) [UNIBO PORTALE](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/UNIBO)
2.	INTERROGARE: #A2 Mediante un ChatBot realizzato con Retrieval Augmented Generation (RAG) si facilita l’interrogazione (ricerca, riassunto, traduzione) di documenti legislativi Europei in italiano e inglese (IGSG, Aptus.AI, UNIBO). [CNR-IGSG](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/CNR-IGSG)
3.	RIASSUMERE: #A3 Mediante un LLM si producono riassunti dei materiali delle audizioni di commissione video finalizzati a certi orientamenti di un particolare interesse (e.g., fornisci un sommario dei punti salienti delle audizioni del giorno orientati al cambiamento climatico.) [UNIBO SOMMARIO VIDEO AUDIZIONI](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/UNIBO/transcriptor).
![Schema generale](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/Screenshot__243_.png)


<a id="call-b"/>

## B.	SUPPORTARE LA PREDISPOSIZIONE DEGLI ATTI DI INIZIATIVA: definizione di strumenti (proof of concept) a supporto della predisposizione di proposte di legge e di atti di indirizzo e di controllo.
### Obiettivo: 
Una volta predisposta la proposta di legge il processo legislativo prevede numerosi passaggi che possono essere facilitati mediante l’utilizzo dell’AI. Si intende creare un set di strumenti via API per agevolare le attività degli uffici.
### Target: 
E’ rivolto agli uffici della Camera dei Deputati che lavorano nel processo di produzione dei disegni di legge, ma anche ai i Servizi di Documentazione della Camera dei Deputati che devono monitorare ex-ante e ex-post l’impatto della legislazione e la sua efficacia. 
 
### Strumenti:
1. ISTRUIRE: #B1 Classificare i testi legislativi simili alla proposta di legge per supportare l’istruttoria conoscitiva usando EUR-LEX, progetti di legge, Normattiva, Corte Costituzionale e darne visualizzazione grafica intellegibile (UNIBO e BitNomos). Suggerire definizioni e cluster di progetti di legge affini. [UNIBO PDL](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/UNIBO)
2. COMPRENDERE: #B2 Riassumere in una sintesi comprensibile gli emendamenti di commissione (LUISS e Asimov AI) [LUISS](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/LUISS).
3. MISURARE: #B3 Verificare l’impatto di una certa policy nella proposta di legge (e.g., climate change) e fornire spiegazione (UNIVER) [UNIVER](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/UNIVR).
L’intero progetto è disponibile e documentato nel seguente portale gitlab pubblico dove si potranno trovare nei diversi branch i documenti utilizzati, i modelli, parte del codice, i risultati, la validazione.
![Schema generale](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/Screenshot__244_.png)
## License
Alcuni prodotti sono rilasciati in CC-by 4.0 International. Altri sono protetti da copyright.

