 **Table of content:**


- [A.	REPERIRE E ORGANIZZARE INFORMAZIONI QUALIFICATE: definizione di strumenti (proof of concept) a supporto delle attività di predisposizione della documentazione per i deputati.](#areperire-e-organizzare-informazioni-qualificate-definizione-di-strumenti-proof-of-concept-a-supporto-delle-attivit%C3%A0-di-predisposizione-della-documentazione-per-i-deputati)

- [B.	SUPPORTARE LA PREDISPOSIZIONE DEGLI ATTI DI INIZIATIVA: definizione di strumenti (proof of concept) a supporto della predisposizione di proposte di legge e di atti di indirizzo e di controllo.](#b-supportare-la-predisposizione-degli-atti-di-iniziativa)


 

# A. REPERIRE E ORGANIZZARE INFORMAZIONI QUALIFICATE

UNIBO propone tre tool:

1. Portale di risorse eterogenee (EUR-LEX, Normattiva, Corte Costituzionale, PDL Camera) tradotti in Akoma Ntoso con tool NLP/AI. In questo modo se si sta scrivendo un nuovo PDL si possono interrogare tutte le fonti sopra citate e verificare se esistono materiali pertinenti che possono essere utili allo studio preliminare l'inizativa. Rispetto ad una tematica inserita dall’utente si chiede al sistema di estrarre i materiali rilevanti utilizzando banche dati eterogenee multilingue (inglese e italiano) quali la legislazione europea (Eur-Lex) e quella nazionale (Normattiva). Si integra in questa ricerca anche la consultazione delle decisioni della Corte Costituzionale. 

- Leos (EUR-LEX): 15283 (2010-2021) regolamenti e direttive europee
- NormaAttiva: 3195 (2010-2024 - aprile)
- Corte Costituzionale 11294 sentenze, 10588 ordinanze (1956-2024)
- PDL: 18^ e 19^ legislature per un totale di 3710 progetti di legge

2. Ricerche avanzate con algoritmi di similitudine utilizzando un approcico ibrido con AI simbolica (regex) e non-simbolica (sentence embedding) e tecniche di information retreival (TF-IDF Lucine index). In particolare per i PDL abbiamo sviluppato una mappatura di similitudine utilizzando le 14 categorie tematiche delle commissioni parlamentari così come fornite dalla manifestazione di interesse. In questo modo se si sta scrivendo un nuovo PDL si possono interrogare tutte le fonti sopra citate e intercettare le definizioni pertinenti o i riferimenti normativi pertinenti.
[Portale principale](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/Screenshot__223_.png)

>a. Aprire il box della ricerca.

>b. Selezionare "Ricerca Avanzata"

>c. Selezionare il repository (LEOS-EUR-LEX; Normattiva, Corte Costituzionale, PDL)

>d. Inserire una keyword del progetto di legge che si intende produrre

>e. Inserire una keyword di definizione che si intende cercare e premere "Cerca definizioni"--> restituisce le definizioni più pertienenti ([Documentazione tecnica](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/UNIBO/definition-annotation))

oppure
>e. Inserire un riferimento normativo parziale "Regulation 406" e premere "Cerca riferimenti" (funziona solo su LEOS)--> restituisce i riferimenti normativi più pertienenti 


![Esempio](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/Screenshot__241_.png)
[Demo](http://u2.cirsfid.unibo.it/portale-camera/#/)


Link ad un documento di Normattiva
![Esempio](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/Screenshot__255_.png)

Se si vogliono visualizzare i grafici si deve premere "Informazioni sul documento" da dentro una pagina singola.

Link al documento 94/2024 della Corte Costituzionale
![Esempio](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/Screenshot__262_.png)

3. Tool di sommarizzazione mediante LLM dei video delle audizioni delle commissioni parlamentari e possiblità di interrogare tali materiali orientati ad un determinato goal (e.g., dammi tutti i frammenti del video di una certa persona che parlano di pesticidi). Mediante un LLM si producono riassunti dei materiali delle audizioni di commissione video finalizzati a certi orientamenti di un particolare interesse (e.g., fornisci un sommario dei punti salienti delle audizioni del giorno orientati al cambiamento climatico.) Serve un account gmail [Demo](https://colab.research.google.com/drive/1dLlg2-jeeegcSJiZcLP2qO41tpS04puc?usp=sharing#scrollTo=Jv6Q95teQeep)

[Documentazione](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/UNIBO/transcriptor)


# B. SUPPORTARE LA PREDISPOSIZIONE DEGLI ATTI DI INIZIATIVA

UNIBO propone un tool per classificare i testi legislativi simili alla proposta di legge per supportare l’istruttoria conoscitiva usando EUR-LEX, progetti di legge, Normattiva, Corte Costituzionale e darne visualizzazione grafica intellegibile (UNIBO e BitNomos). Suggerire definizioni e cluster di progetti di legge affini.
Per formare i cluster tematici si sono utilizzate le 14 categorie tematiche delle commissioni parlamentari così come fornite dalla manifestazione di interesse. In questo modo scrivendo le parole chiave di un nuovo PDL si possono recuperare tutti i PDL che sono simili o pertinenti a quello che si intende proporre.

>a. Aprire il box della ricerca

>b. Selezionare "Ricerca Avanzata"

>c. Selezionare il repository (PDL)

>d. Inserire una keyword del progetto di legge che si intende produrre

>e. Inserire una keyword tematica che si intende cercare e premere "Cerca Argomento"--> restituisce i PDL più pertienenti 

![Esempio](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/Screenshot__258_.png)

[Demo](http://u2.cirsfid.unibo.it/portale-camera/#/)

[Documentazione](https://gitlab.com/CIRSFID/genai4lex/-/tree/main/UNIBO/similarity)
