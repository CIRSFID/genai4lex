# USAGE: python3 definitionAnnotation_Camera.py input_folder output_folder
# converts a folder of .xml files to .xml files with annotated definitions

# Use this file to find text patterns (using regex) quickly...
import os
import re
import sys
from pathlib import Path

from lxml import etree

AKN_NAMESPACE = "http://docs.oasis-open.org/legaldocml/ns/akn/3.0/WD17"
FMX4_NAMESPACE = "http://formex.publications.europa.eu/schema/formex-05.56-20160701.xd"
XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance"
AKN_PREFIX = "{"+AKN_NAMESPACE+"}"
FMX_PREFIX = "{"+FMX4_NAMESPACE+"}"
AKN_PREFIX_XPATH = {"akomaNtoso": AKN_NAMESPACE}


def add_definitions(input_folder, output_folder):
  
  reg2=re.compile("((?P<left>(^\s{0,}([a-z]\)\s)?([p|P]er\s)?(\s{0,})?))(?P<definiendum>((.+?(?=((,)?(\s)?sono considerat[i|e])|((,)?(\s)?propriamente dett[i|a|o|e])|((,)?(\s)?si intend[a-z]{1,})|(:))))|(.+?(?=,))|(.+?(?=(,)?(\s)?e')))(?P<definiens>((.|\s)*)))")
  reg_check1=re.compile("^((?!quelli))(.|\s)*$")
  all_akn_files = list(Path(input_folder).rglob("*.xml"))
  print("File in input: ",len(all_akn_files))
  print()
  
  for akn_file in all_akn_files:
   try:  
      #print(akn_file)
      if not (str(akn_file).endswith(".akn") or str(akn_file).endswith(".xml")):
          continue
      num_value_new=0
      defs=[]
      refs=[]
      subrefs=[]

        
      output_found = False

      akn_as_xml = etree.parse(akn_file)
      article=akn_as_xml.xpath(".//t:article[contains(./t:heading,'Definizioni')]",namespaces={'t': AKN_NAMESPACE})
      for i in article:
        if i is not None:
            this_sibling2=i.findall("./t:content/t:blockList/t:item",namespaces={'t': AKN_NAMESPACE})
            if this_sibling2:
              for el in this_sibling2:
                conc_list=el.getchildren()
                for element in conc_list:
                  if element.tag==AKN_PREFIX+"p" or element.tag==AKN_PREFIX+"blockList":
                      if element.tag==AKN_PREFIX+"blockList":
                        x=element.find("./t:listIntroduction",namespaces={'t': AKN_NAMESPACE})
                      else:   
                        x=element
                      #  x is p or list introduction
                      if x is not None :
                        
                        mod=x.findall("./t:mod",namespaces={'t': AKN_NAMESPACE})
                        for m in mod:
                           quotedText=m.find("./t:quotedText",namespaces={'t': AKN_NAMESPACE})
                           if quotedText is not None and len(m.getchildren())==1:
                              if quotedText.text is None:
                                 quotedText.text=""
                              if m.tail is None:
                                 m.tail=""
                              m.tail=quotedText.text+m.tail
                        etree.strip_elements(x,'{http://docs.oasis-open.org/legaldocml/ns/akn/3.0/WD17}mod',with_tail=False)    
                          
                        check_1=reg_check1.search(x.text)
                        results=reg2.search(x.text)
                        if results is not None and check_1 is not None:
                          second_parent = x.getparent() #item o blocklist
                          num_value_new+=1
                          if second_parent.tag==AKN_PREFIX+'blockList':
                            counter=0
                            for p_tag in second_parent.findall(".//t:p",namespaces={'t': AKN_NAMESPACE}):
                                # Process each <p> tag as needed
                                #print(f"Found <p> tag inside def_el: {ET.tostring(p_tag, encoding='utf-8').decode('utf-8')}")
                                #print(p_tag.getparent())
                                if p_tag.getparent().tag == AKN_PREFIX+'authorialNote':
                                  print("authorialNote Found!")
                                  continue

                                counter +=1
                                # Extract text content from <p> tag
                                p_text = p_tag.text
                                

                                
                                if p_text is not None and len(p_text)>0:                                  
                                    subrefs.append((str(num_value_new), "#defBody_" + str(num_value_new) + "-" + str(counter)))
                                    # Create a new XML element for the new tag (e.g., <defBody>)
                                    new_tag = etree.Element(AKN_PREFIX + "defBody", eId="defBody_" + str(num_value_new) +"-"+str(counter))
                                    new_tag.text = p_text
                                    for ch in p_tag.getchildren():
                                       new_tag.append(ch)
                                    p_tag.text = ""
                                    p_tag.append(new_tag)

                          def_text = results.group("definiendum")
                          left=results.group("left")
                          #print(def_text)
                          # get the text of the defBody
                        
                          defbody_text = results.group("definiens")
                          #print(defbody_text)
                          
                          #print()
                          # we create a new <def> element


                          def_el = etree.Element(AKN_PREFIX+"def", eId='def_'+ str(num_value_new))
                          def_el.text = def_text.strip()
                          defs.append(re.sub(r"\"|\“|\”|\‘|\’|\„|\”|\<|\>|\«|\»","",def_el.text))#showas
                          refs.append('#def_'+ str(num_value_new))
                          second_parent.set('defines', '#def_'+ str(num_value_new))


                          # creating new <defBody> element
                          defbody_el= etree.Element(AKN_PREFIX+"defBody",eId= "defBody_" + str(num_value_new))
                          defbody_el.text = defbody_text.strip()

                          # activating the flag to print the output
                          output_found = True

                          for child_el in x.getchildren():
                              if child_el.tag!=AKN_PREFIX+"ins":
                                defbody_el.append(child_el)

                          x.text = left
                          x.insert(0,def_el)          
                          x.insert(1,defbody_el)           



      path= str(akn_file)
      file_name = path.split("/")[-1]
      file_name=file_name.split(".")

      defss=[]
      for word in defs:
        words = word.split(" ")
        words[1:] = [w.title() for w in words[1:]]
        defss.append(re.sub(r"\'|\"|\“|\”|\‘|\’|\„|\”|\«|\,|\<|\>|\»","","".join(words)))
      # Adding DefinitionHead And Body After ActiveModification Tag
      analysistag=akn_as_xml.find(".//t:meta/t:analysis",namespaces={'t': 'http://docs.oasis-open.org/legaldocml/ns/akn/3.0'})
      if analysistag is None:
         meta=akn_as_xml.find(".//t:meta",namespaces={'t': AKN_NAMESPACE})
         references=akn_as_xml.find(".//t:references",namespaces={'t': AKN_NAMESPACE})
         analysistag=etree.Element(AKN_PREFIX+"analysis",source= "")
         if references is not None:
          index=meta.index(references)
          meta.insert(index,analysistag)
         else: 
            meta.append(analysistag)
      if analysistag is not None:
        analysistag.tail = '\n'
        def_el = etree.Element(AKN_PREFIX + "definitions", source="#unibo")
        def_el.text = '\n  '
        def_el.tail = '\n       '
        for i in range(len(defs)):
            grandchild_el = etree.SubElement(def_el, AKN_PREFIX + "definition")
            grandchild_el.set("refersTo", "#" +defss[i])
            grandchild_el.text = '\n    '
            grandchild_el.tail = '\n'
            sub_grandchild_el = etree.SubElement(grandchild_el, AKN_PREFIX + "definitionHead")
            sub_grandchild_el.set("href", refs[i])
            sub_grandchild_el.set("refersTo", "#" + defss[i])
            sub_grandchild_el.tail = '\n'
            defbody=refs[i].split("_")

            new_sub_grandchild_el = etree.SubElement(grandchild_el, AKN_PREFIX + "definitionBody")
            new_sub_grandchild_el.set("href","#defBody_"+defbody[1])
            new_sub_grandchild_el.tail = '\n'
            newcount = 0
            for x in range(len(subrefs)):
              if int(defbody[1]) == int(subrefs[x][0]):
                newcount+=1
                new_sub_grandchild_el = etree.SubElement(grandchild_el, AKN_PREFIX + "definitionBody")
                new_sub_grandchild_el.set("href",subrefs[x][1])
                new_sub_grandchild_el.tail = '\n'
          
        analysistag.append(def_el)
      else:
        print("Analysis not found!")
      # Adding TCLterm inside Reference Tag
      references=akn_as_xml.find(".//t:references",namespaces={'t': AKN_NAMESPACE})
      if references is not None:
          num_tags = len(references)
          for i in range(len(defs)):
              def_el = etree.Element(AKN_PREFIX + "TLCTerm", eId=defss[i],href="/akn/ontology/terms/it/"+defss[i],showAs=defs[i])
              references.insert(num_tags+i, def_el)
              def_el.tail = '\n'

      if output_found:

        OUT_DIR = output_folder              
        etree.indent(akn_as_xml, space=" ")

        if not os.path.isdir(OUT_DIR):
          os.mkdir(OUT_DIR)
        
        #print("parsing: ", str(akn_file).split("/")[-1])


        out_file_path = os.path.join(OUT_DIR, str(akn_file).split("/")[-1]) 
        with open(out_file_path, "wb") as f:

          akn_as_xml.write(f,xml_declaration=True,encoding='utf-8',
                method="xml")
          #print("file saved at: ", out_file_path)
          #print("")

   except:
     print(f'''Failed: {akn_file}''')
  all_akn_files = list(Path(output_folder).rglob("*.xml"))
  print("File annotati: ",len(all_akn_files))

  
if __name__ == "__main__":
  
  # parsing arguments
  input_folder = sys.argv[1]
  output_folder = sys.argv[2]


  add_definitions(input_folder, output_folder)
  
  exit(0)