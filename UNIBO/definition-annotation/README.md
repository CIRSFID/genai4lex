# Suggeritore di definizioni normative pertinenti

Il presente modulo suggerisce le definizioni normative presenti in un corpus normativo più pertinenti rispetto al tema che l'utilizzatore intende investigare. Spesso le definizioni normative risultano inconsistenti o duplicate o sovrapposte nei termini (termini uguali- significati diversi; termini diversi - significati uguali).

Utilizzando in ingresso keyword tematiche (e.g., EUROVOC) e la definizione normativa che si intende cercare il sistema utilizzando strumenti di indicizzazione (modifica di Lucine), un database SQL di riferimenti normativi per risolvere le citazioni contenute nelle definizioni, un algoritmo di similitudine per attribuire un punteggio di pertinenza ai documenti.

![Schema generale](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/definizioni3.png)

Si indicizzano le definizioni normative di tutti i documenti: definines+definiendum+EUROVOC (TF-IDF).

La classe Similarity di Lucine implementa il modello di attribuzione del punteggio. Prende in considerazione le statistiche sul corpus contenute nell’indice, il numero di termini che corrispondono rispetto alla query e dei fattori moltiplicativi di potenziamento
espressi nella ricerca. Questa classe viene sfruttata anche dalla catena del processo di indicizzazione, poich´e si occupa del calcolo dei fattori di normalizzazione, che dipendono dalla lunghezza dei campi e dai fattori di boost specificati nella configurazione.
Lucene offre diverse implementazioni gi`a realizzate della classe Similarity, che rispecchiano i diversi modelli matematici di valutazione del punteggio creati nel campo dell’Information Retrieval.
In particolare, in questo progetto viene fatto largo uso dei metodi offerti dalla classe DefaultSimilarity che tuttavia è stata modificata per aderire meglio al dominio giuridico.

![Architetture](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/definizioni.png)

Le definizioni normative sono composte da due parti: definines e definiendum, ma spesso contengono anche altri riferimenti normativi minando così l'approccio basato sulla frequenza delle parole. Per questo occorre navigare il riferimento normativo, recuperare il testo e indicizzarlo.

![Anatomia di una definizione normativa](https://gitlab.com/CIRSFID/genai4lex/-/raw/main/UNIBO/similarity/images/definizione2.png)

In caso di testi con più versioni consolidate nel tempo, è possibile filtrare anche secondo un asse temporale che può essere nel passato, nel presente o anche nel futuro, considerando che il processo legislativo può durare anni è interessante avere una vista proiettata nella simulazione futura applicando così le modifiche che saranno in vigore nel futuro.

