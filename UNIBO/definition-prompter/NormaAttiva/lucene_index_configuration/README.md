## Creazione degli indici Lucene su eXist
La creazione degli indici permette di effettuare la ricerca di termini full-text su elementi di tipo "def" e "defBody" e di ottenere un punteggio di rilevanza testuale, calcolato grazie alle strutture dati costruite dall'indicizzazione di Lucene.


#### Campi del documento indicizzati per ogni definizione

- **def**: Contenuto testuale dell'elemento AkomaNtoso "def".
- **defBodies**: Contenuto testuale degli elementi AkomaNtoso "defBody".
- **ref**: Navigazione dei riferimenti contenuti nel definiens di una definizione.
- **partition**: Identificativo univoco dell'elemento XML contenente la definizione.
- **longTitle**: Contenuto testuale dell'elemento AkomaNtoso "docTitle".
- **FRBRExpressionuri**: Uri FRBRExpression del documento contenente la definizione.
- **docNumber**: Numero del documento contenente la definizione.
- **date**: Data relativa al campo FRBRExpression del documento che contiene la definizione.



