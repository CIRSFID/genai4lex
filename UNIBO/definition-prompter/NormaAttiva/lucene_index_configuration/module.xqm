xquery version "3.1";
module namespace idx="http://exist-db.org/lucene/test/";
import module namespace console="http://exist-db.org/xquery/console";
declare namespace akoma = "http://docs.oasis-open.org/legaldocml/ns/akn/3.0";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";


declare function idx:put-defBody($def as element(akoma:def)) as xs:string {
try{
let $doc:=root($def)
let $definition:=$doc//akoma:definition[./akoma:definitionHead/@href=concat("#",$def/@eId)]
let $count:=count($definition/*)
let $countdef:=count($definition)
return if($countdef=1)
then(
let $ids:=for $str in $definition/akoma:definitionBody/@href[contains(.,"defBody_")] return substring($str,2)
return normalize-space(string-join($doc//akoma:defBody[@eId=$ids]," "))
)
else(concat("",""))
} catch * {
let $g:=console:log("error",concat("defbody: ",$err:code,$err:description,$err:value))
return concat("","")
}
};
declare function idx:put-partition($def as element(akoma:def)) as xs:string {
try{
let $partition:=$def/ancestor::akoma:point/@eId[last()]
return concat($partition,"")
} catch * {
let $g:=console:log("error",concat("partition: ",$err:code,$err:description,$err:value))
return concat("","")    
    
}
};
declare function idx:put-longTitle($def as element(akoma:def)) as xs:string {
try{
let $longTitle:=normalize-space(root($def)//akoma:docTitle/string())
return concat($longTitle[1],"")
} catch * {
let $g:=console:log("error",concat("longtitle: ",$err:code,$err:description,$err:value))
return concat("","")    
    
}

};
declare function idx:put-uri($def as element(akoma:def)) as xs:string {
try{
let $uri:=root($def)//akoma:FRBRExpression/akoma:FRBRuri/@value/string()
return concat($uri[1],"")
} catch * {
let $g:=console:log("error",concat("uri: ",$err:code,$err:description,$err:value))
return concat("","")    
    
}

};

declare function idx:put-date($def as element(akoma:def)) as xs:date {
try{
let $doc := root($def)//akoma:FRBRExpression/akoma:FRBRdate/@date
return $doc[1]
} catch * {
let $g:=console:log("error",concat("date: ",$err:code,$err:description,$err:value))
return xs:date("1970-01-01")
}
};
declare function idx:put-docNumber($def as element(akoma:def)) as xs:string {
try{
let $doc := root($def)//akoma:FRBRWork/akoma:FRBRuri/@value
let $tokens:= tokenize($doc[1],"/")
return $tokens[last()]
} catch * {
let $g:=console:log("error",concat("docNumber: ",$err:code,$err:description,$err:value))
return concat("","")
}
};
declare function idx:put-ref($def as element(akoma:def)) as xs:string {
try{
let $doc:=root($def)
let $definition:=$doc//akoma:definition[./akoma:definitionHead/@href=concat("#",$def/@eId)]
let $count:=count($definition/*)
let $countdef:=count($definition)
return if($countdef=1)
then(

let $ids:=for $str in $definition/akoma:definitionBody/@href[contains(.,"defBody_")] return substring($str,2)

let $defB:=$doc//akoma:defBody[@eId=$ids]

return normalize-space(string-join(idx:find-ref($defB//akoma:ref[not(exists(./ancestor::akoma:authorialNote))]/@href)," "))


)
else(concat("",""))
} catch * {
let $g:=console:log("error",concat("putref: ",$err:code,$err:description,$err:value))
return concat("","")    
    
}
};
declare function idx:find-ref($ref as xs:string*) as xs:string {

try{
let $ref1:=distinct-values($ref[contains(.,"/akn/")]) 
let $log1:=console:log("default1",string-join($ref1," # "))
let $dd:=
for $r in $ref1
let $t:=tokenize($r,"#")
return if(count($t)>1)

then(

let $req:=$t[1]
let $this:= if(contains($req,"/eu/"))
    then(idx:query-db($req,1,1))
    else(
        if(contains($req,"/bill/"))
        then(idx:query-db($req,2,3))
        else(
            if(contains($req,"/act/"))
            then(idx:query-db($req,2,2))
            else(concat("",""))
            
            )
        
        )
return if (boolean($this))

then(
let $h:=$t[2]
let $p:=document{doc($this)}

let $riferimento:=$p//*[matches(@eId,concat(".*(",$h,")$"))][1]
let $log:=console:log(concat("Riferimento: ",$r,", Token1: ",$t[1],", Token2: ",$t[2],", Input: ",$req,", Risposta : ",$this,", partition: ",$h,", countdocs:",count($p),", countref: ",count($riferimento)))
return if($riferimento)
then concat("<ref><href>",$this,"~",$h,"</href>","<content>",$riferimento,"</content></ref>")
else concat("","")
(:  :return normalize-space(string-join($cip)):)

)
else(concat("",""))
    
    
    
)

else(concat("",""))










return string-join($dd," ")
} catch * {
let $g:=console:log("error",concat("findref: ",$err:code,$err:description,$err:value))
return concat("","")    
    
}
};
declare function idx:query-db($ref as xs:string,$field_to_query as xs:int,$server_to_query as xs:int) as xs:string {
try{
let $mode:=("AliasAKNshort","FRBRWORKTHIS")
let $server:="http://u2.cirsfid.unibo.it/node/leos-server/componentdb"
let $db:=("leos_data","NormattivaDB","CameraDB")
let $collection:=("/db/Leos/Documents/","/db/NormaAttiva/Documents/","/db/portal-camera/Documents/")    
let $payload := map {
$mode[$field_to_query]: $ref,
"database": $db[$server_to_query]
  }
let $res:=hc:send-request(
  <hc:request method='post'>
    <hc:body media-type="application/json" method="text"/>
  </hc:request>,
  $server,
  serialize($payload, <output:serialization-parameters>
            <output:method>json</output:method>
        </output:serialization-parameters>)
)[2] => util:base64-decode() => parse-json()
(: MODIFICARE URL SERVIZIO :)
return if(array:size($res)>0)
then(
    let $date :=format-dateTime(xs:dateTime( $res?1?ValidityDate),"[Y]-[M01]-[D01]")
    let $docname := $res?1?AliasCELEX
    return concat($collection[$server_to_query],$docname,".xml")
)
else( concat("","") )
} catch * {
let $g:=console:log("error",concat("querydb: ",$err:code,$err:description,$err:value))
return concat("","")    
    
}
};
