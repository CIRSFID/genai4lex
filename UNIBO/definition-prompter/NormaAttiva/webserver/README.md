## REST API
Lo script utilizza la libreria pyexistdb per effettuare query al database eXist. Il database contiene documenti normativi italiani in AkomaNtoso. La collezione di documenti è stata indicizzata con la libreria Java Lucene per poter ottenere un punteggio di rilevanza testuale tra i termini di ricerca inseriti e le definizioni indicizzate. Inoltre, grazie all'indicizazzione, è possibile costruire campi di testo aggiuntivi associati alle definizioni, come ad esempio quelli relativi alla navigazione dei riferimenti contenuti nel definiens, i quali possono essere successivamente restituiti nella risposta.
#### Richiesta

- **URL**: `http://127.0.0.1:9644/suggest`
- **Metodo**: POST
- **Headers**: `Content-Type: application/json`
- **Body**: JSON

##### Parametri della richiesta

- `term`: (string) Una termine arbitrario, indicato dall’utente, per cui si vuole ottenere una definizione.
- `date`: (string) Data per filtrare le definizioni. Le definizioni restituite saranno precedenti a questa data. Formato: `YYYY-MM-DD`. Se non specificata o vuota, non viene applicato alcun filtro.

##### Esempio di richiesta

```sh
 curl -X POST -H "Content-Type: application/json" -d '{"term": "autorita competente", "date":"2023-04-03"}' http://127.0.0.1:9644/suggest
 ```


 #### Risposta

```
{
    "response": {
        "result": [
            {
                "defRes": {
                    "FRBRExpressionuri": "/akn/it/act/decretoDelPresidenteDellaRepubblica/stato/2013-03-13/59/ita@2023-04-03",
                    "longTitle": "Regolamento recante la disciplina dell'autorizzazione unica ambientale e la semplificazione di adempimenti amministrativi in materia ambientale gravanti sulle piccole e medie imprese e sugli impianti non soggetti ad autorizzazione integrata ambientale, a norma dell'articolo 23 del decreto-legge 9 febbraio 2012, n. 5, convertito, con modificazioni, dalla legge 4 aprile 2012, n. 35. (13G00101)",
                    "versionDate": "2023-04-03",
                    "partition": "art_2__para_1.__point_b",
                    "def": "autorita' competente",
                    "defBody": ": la Provincia o la diversa autorita' indicata dalla normativa regionale quale competente ai fini del rilascio, rinnovo e aggiornamento dell'autorizzazione unica ambientale, che confluisce nel provvedimento conclusivo del procedimento adottato dallo sportello unico per le attivita' produttive, ai sensi dell'articolo 7 del decreto del Presidente della Repubblica 7 settembre 2010, n. 160, ovvero nella determinazione motivata di cui all'articolo 14-ter, comma 6-bis, della legge 7 agosto 1990, n. 241;",
                    "refs": {
                        "ref": {
                            "href": "/db/NormaAttiva/Documents/20100930_010G0183_VIGENZA_20211218.xml~art_7",
                            "content": "Art. 7. Procedimento unico 1. Fuori dei casi disciplinati dal Capo III, le istanze per l'esercizio delle attivita' di cui all'articolo 2, comma 1, sono presentate al SUAP che, entro trenta giorni dal ricevimento, salvi i termini piu' brevi previsti dalla disciplina regionale, puo' richiedere all'interessato la documentazione integrativa; decorso tale termine l'istanza si intende correttamente presentata. 2. Verificata la completezza della documentazione, il SUAP adotta il provvedimento conclusivo entro trenta giorni, decorso il termine di cui al comma 1, salvi i termini piu' brevi previsti dalla normativa regionale, ((...)). 3. Quando e' necessario acquisire intese, nulla osta, concerti o assensi di diverse amministrazioni pubbliche, il responsabile del SUAP ((indice)) una conferenza di servizi ai sensi e per gli effetti previsti dagli articoli da 14 a 14-quinquies della legge 7 agosto 1990, n. 241, ovvero dalle altre normative di settore, ((...)). ((PERIODO SOPPRESSO DAL D.LGS. 30 GIUGNO 2016, N. 127)). Scaduto il termine di cui al comma 2, ovvero in caso di mancato ricorso alla conferenza di servizi, si applica l'articolo 38, comma 3, lettera h), del decreto-legge. 4. ((COMMA ABROGATO DAL D.LGS. 30 GIUGNO 2016, N. 127)). 5. Nei procedimenti di cui al comma 1, l'Agenzia, su richiesta del soggetto interessato, puo' svolgere attivita' istruttoria ai sensi dell'articolo 38 comma 3, lettera c), del decreto-legge, e trasmette la relativa documentazione, in via telematica, al responsabile del SUAP. L'Agenzia fornisce assistenza per l'individuazione dei procedimenti da attivare in relazione all'esercizio delle attivita' produttive o alla realizzazione degli impianti produttivi, nonche' per la redazione in formato elettronico delle domande, dichiarazioni e comunicazioni ed i relativi elaborati tecnici. Se il comune lo consente, l'Agenzia puo' fornire supporto organizzativo e gestionale alla conferenza di servizi. 6. Il provvedimento conclusivo del procedimento, assunto nei termini di cui agli articoli da 14 ((a 14-quinquies)) della legge 7 agosto 1990, n. 241, e', ad ogni effetto, titolo unico per la realizzazione dell'intervento e per lo svolgimento delle attivita' richieste. 7. Il rispetto dei termini per la conclusione del procedimento costituisce elemento di valutazione del responsabile del SUAP e degli altri soggetti pubblici partecipanti alla conferenza di servizi."
                        }
                    },
                    "score": "22.002254"
                }
            }
            
        ]
    }
}
```
