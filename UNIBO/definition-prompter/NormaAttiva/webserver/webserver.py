from flask import Flask, request, jsonify
from pyexistdb import db
import xmltodict
import json
from datetime import datetime

__author__ = 'generosolongo'
EXISTDB_SERVER_PASSWORD = '####'
EXISTDB_SERVER_URL = "http://u2.cirsfid.unibo.it:8080/exist/"
EXISTDB_ROOT_COLLECTION = "/db/NormaAttiva/Documents"
EXISTDB_SERVER_USER = '####'

app = Flask(__name__)

@app.route('/suggest', methods=['POST'])
def process_request():
    data = request.get_json()

    term = data.get('term')
    eurovoc = data.get('eurovoc',[])
    date=data.get("date")
    if term is not None:
        result = execute_query(term, eurovoc, date)
    else:
        result=jsonify({'response': "Error."})
    return result

def execute_query(term, eurovoc, date):
    querydate=""
    queryclass=""
    querydef=""
    def_terms=term.split(" ")

    if len(def_terms)>1:
        for k in range(0,2):
            querydef+=f"""or ft:query(.,'{def_terms[k]}',$options) or 
                    ft:query(.,'defBodies:{def_terms[k]}',$options) """    

    
    if(date is not None):
        try:
            date_obj=datetime.strptime(date,"%Y-%m-%d").date()
            date_validated=date_obj.strftime("%Y-%m-%d")
            querydate=f"""[ft:field(.,"date","xs:date")<=xs:date("{date_validated}")]"""
        except:
            querydate=""
            
            

    # Connect to eXist-db
    connection = db.ExistDB(server_url=EXISTDB_SERVER_URL,username=EXISTDB_SERVER_USER,password=EXISTDB_SERVER_PASSWORD)
    # Define your XQuery

    xquery =f"""
    declare namespace akoma = "http://docs.oasis-open.org/legaldocml/ns/akn/3.0";
    declare variable $options as map(*) := map {{ "fields" : ("FRBRExpressionuri","longTitle","partition","defBodies","date","ref") }};


    for $defs in collection("{EXISTDB_ROOT_COLLECTION}")//akoma:def[
        ft:query(.,'"{term}"~10',$options) or 
        ft:query(.,'defBodies:"{term}"~10',$options)
        {querydef}
        ]{querydate}

    """ """
    
    
    let $id:=ft:field($defs,"FRBRExpressionuri")
    let $score as xs:float := ft:score($defs)
    order by $score descending

    return(
            <defRes>
            <FRBRExpressionuri>{$id}</FRBRExpressionuri> 
            <longTitle>{ft:field($defs,"longTitle")}</longTitle>
            <versionDate>{ft:field($defs,"date","xs:date")}</versionDate>


            

            <partition>{ft:field($defs,"partition")}</partition>
            <def>{$defs/string()}</def>
            <defBody>{ft:field($defs,"defBodies")}</defBody>
            {parse-xml(concat("<refs>",ft:field($defs,"ref"),"</refs>"))}
            <score>{$score}</score>
            </defRes>
    )  
    """
    print(xquery)

    result = connection.executeQuery(xquery)
    hits=connection.getHits(result) 
    if hits:
             
        x=hits
        definizione="<response>"
        for i in range(0,x):
            definizione=definizione+"<result>"+str(connection.retrieve(result,i))+"</result>"
        definizione=definizione+"</response>"
        xml_dict = xmltodict.parse(definizione)
        res = json.dumps(xml_dict)
    else: 
        res=jsonify({'response': "Your search did not match any documents."})


    connection.releaseQueryResult(result)
    return res
def to_camel_case(input_string):
    words = input_string.split()
    camel_case_words = [words[0]] + [word.capitalize() for word in words[1:]]
    camel_case_string = ''.join(camel_case_words)
    return camel_case_string
if __name__ == '__main__':
    app.run(debug=True,port=9644)
