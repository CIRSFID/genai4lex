## REST API
Lo script utilizza la libreria pyexistdb per effettuare query al database eXist. Il database contiene Progetti di Legge della Camera dei Deputati in AkomaNtoso. La collezione di documenti è stata indicizzata con la libreria Java Lucene per poter ottenere un punteggio di rilevanza testuale tra i termini di ricerca inseriti e le definizioni indicizzate. Inoltre, grazie all'indicizazzione, è possibile costruire campi di testo aggiuntivi associati alle definizioni, come ad esempio quelli relativi alla navigazione dei riferimenti contenuti nel definiens, i quali possono essere successivamente restituiti nella risposta.
#### Richiesta

- **URL**: `http://127.0.0.1:9645/suggest`
- **Metodo**: POST
- **Headers**: `Content-Type: application/json`
- **Body**: JSON

##### Parametri della richiesta

- `term`: (string) Una termine arbitrario, indicato dall’utente, per cui si vuole ottenere una definizione.
- `date`: (string) Data per filtrare le definizioni. Le definizioni restituite saranno precedenti a questa data. Formato: `YYYY-MM-DD`. Se non specificata o vuota, non viene applicato alcun filtro.

##### Esempio di richiesta

```sh
 curl -X POST -H "Content-Type: application/json" -d '{"term": "autorita competente", "date":"2019-10-12"}' http://127.0.0.1:9645/suggest
 ```


 #### Risposta

```
{
    "response": {
        "result": [
            {
                "defRes": {
                    "FRBRExpressionuri": "/akn/it/bill/propostaDiLegge/2019-10-11/2167/ita@",
                    "longTitle": "XVIII LEGISLATURA CAMERA DEI DEPUTATI N. 2167 PROPOSTA DI LEGGE d'iniziativa dei deputatiMAGI, DE FILIPPOIstituzione del voucher universale per i servizi alla persona e alla famigliaPresentata l'11 ottobre 2019 PROPOSTA DI LEGGE",
                    "versionDate": "2019-10-11",
                    "partition": "art_2__content__list_1__item_d",
                    "def": "servizi competenti",
                    "defBody": ": i servizi pubblici e privati per il lavoro ovvero i centri per l'impiego di cui all'articolo 18 del decreto legislativo 14 settembre 2015, n. 150, come modificato dall'articolo 11 della presente legge, e gli altri organismi autorizzati o accreditati a svolgere le funzioni previste, in conformita' alle norme delle regioni e delle province autonome di Trento e di Bolzano;",
                    "refs": {
                        "ref": {
                            "href": "/db/NormaAttiva/Documents/20150923_15G00162_VIGENZA_20230816.xml~art_18",
                            "content": "Art. 18. Servizi e misure di politica attiva del lavoro 1. Allo scopo di costruire i percorsi piu' adeguati per l'inserimento e il reinserimento nel mercato del lavoro, le regioni e le province autonome di Trento e Bolzano costituiscono propri uffici territoriali, denominati centri per l'impiego, per svolgere in forma integrata, nei confronti dei disoccupati, lavoratori beneficiari di strumenti di sostegno al reddito in costanza di rapporto di lavoro e a rischio di disoccupazione, le seguenti attivita': a) orientamento di base, analisi delle competenze in relazione alla situazione del mercato del lavoro locale e profilazione; b) ausilio alla ricerca di una occupazione, anche mediante sessioni di gruppo, entro tre mesi dalla registrazione; c) orientamento specialistico e individualizzato, mediante bilancio delle competenze ed analisi degli eventuali fabbisogni in termini di formazione, esperienze di lavoro o altre misure di politica attiva del lavoro, con riferimento all'adeguatezza del profilo alla domanda di lavoro espressa a livello territoriale, nazionale ed europea; d) orientamento individualizzato all'autoimpiego e tutoraggio per le fasi successive all'avvio dell'impresa; e) avviamento ad attivita' di formazione ai fini della qualificazione e riqualificazione professionale, dell'autoimpiego e dell'immediato inserimento lavorativo; f) accompagnamento al lavoro, anche attraverso l'utilizzo dell'assegno individuale di ricollocazione; g) promozione di esperienze lavorative ai fini di un incremento delle competenze, anche mediante lo strumento del tirocinio; h) gestione, anche in forma indiretta, di incentivi all'attivita' di lavoro autonomo; i) gestione di incentivi alla mobilita' territoriale; l) gestione di strumenti finalizzati alla conciliazione dei tempi di lavoro con gli obblighi di cura nei confronti di minori o di soggetti non autosufficienti; m) promozione di prestazioni di lavoro socialmente utile, ai sensi dell'articolo 26 del presente decreto. 2. Le regioni e le province autonome svolgono le attivita' di cui al comma 1 direttamente ovvero, con l'esclusione di quelle previste dagli articoli 20 e 23, comma 2, mediante il coinvolgimento dei soggetti privati accreditati sulla base dei costi standard definiti dall'ANPAL e garantendo in ogni caso all'utente facolta' di scelta. 3. Le norme del presente Capo si applicano al collocamento dei disabili, di cui alla legge n. 68 del 1999, in quanto compatibili. Note all'art. 18: Per il testo della citata legge n. 68 del 1999, si vedano le note all'articolo 1."
                        }
                    },
                    "score": "29.753052"
                }
            }
        ]
    }
}
```
