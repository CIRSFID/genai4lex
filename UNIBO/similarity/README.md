# Classificazione per argomenti

## Introduzione e Obiettivi

L'obiettivo di questo componente è quello di coadiuvare nella stesura di
documenti normativi, in particolare fornendo informazioni su eventuali
altri documenti rilevanti e sugli argomenti più pertinenti. Lo strumento
è in grado di operare su 4 insiemi di documenti in formato Akoma Ntoso
XML:

-   Normattiva: un insieme di documenti estratti dal portale
    normattiva.it, che contiene i documenti normativi italiani;

-   PDL: un insieme di progetti di legge italiani delle legislature
    XVIII e XIX;

-   Corte Costituzionale: le ordinanze e sentenze della corte
    costituzionale italiane;

-   LEOS: si tratta di documenti normativi provenienti dall'Unione
    Europea, in lingua inglese.

Poiché si tratta di basi documentali molto eterogenee, abbiamo scelto di
utilizzare vari argomenti per categorizzare i documenti:

-   Le commissioni della camera sono state utilizzate per classificare i documenti italiani. Per quanto riguarda la sola corte
    costituzionale, sono state rimosse le commissioni \"Giustizia\" e
    \"Affari costituzionali, della Presidenza del consiglio e interni
    della Camera dei deputati\" in quanto esse sono applicabili alla
    stragrande maggioranza dei documenti della Corte Costituzionale.

-   Per quanto riguarda i documenti LEOS, abbiamo sfruttato il thesaurus europeo multilingue, [EuroVoc](https://eur-lex.europa.eu/browse/eurovoc.html), sfruttando le parole
    più in alto nella gerarchia e i termini che ne discendono direttamente. Dai
    termini di livello massimo è stato rimosso \"Unione Europea\", che,
    in modo analogo a quanto discusso per la Corte Costituzionale, è
    pertinente a tutti i documenti di questa base documentale.

Il primo passaggio è dunque quello di creare rappresentazioni vettoriali
dense per i documenti dei tre dataset.

## Rappresentazioni vettoriali dei documenti

Per poter effettuare una classificazione dei documenti legislativi
abbiamo operato sfruttando [Sentence Transformers](https://sbert.net/) per creare sentence
embeddings a partire dal contenuto testuale di tali documenti. In
particolare, abbiamo sfruttato il modello multilingue
"[paraphrase-multilingual-mpnet-base-v2](https://huggingface.co/sentence-transformers/paraphrase-multilingual-mpnet-base-v2)". Tuttavia, la lunghezza dei
documenti legislativi non consente di applicare direttamente un
approccio pensato per la similarità semantica fra frasi a interi
documenti. Per tale ragione, abbiamo optato per un approccio che sfrutta
la struttura gerarchica dei documenti legislativi e la rappresentazione
degli stessi tramite AkomaNtoso XML, in particolare considerando i
titoli e gli articoli che compongono i documenti, per creare una
classificazione dei documenti.

La derivazione delle rappresentazioni vettoriali per i documenti
legislativi italiani ed europei è stata effettuata seguendo la seguente
procedura:

1.  Vengono individuati tutti gli articoli del documento;

2.  A partire da ciascun articolo, viene attraversato l'albero XML per
    individuare elementi XML che non siano considerabili come \"inline\"
    (quali riferimenti, date, etc). In questo modo viene garantito che
    le frasi che compongono gli articoli non vengano separate, allo
    stesso tempo sfruttando gli elementi strutturali dei documenti
    legislativi (paragrafi, punti di liste, etc).

3. Vengono risolti tutti i riferimenti normativi presenti in ciascun elemento e viene ottenuto il testo corrispondente. Nel caso di riferimenti ad un intero documento viene usato il titolo e il primo articolo. 

4.  Viene infine calcolata una rappresentazione vettoriale per ciascun
    articolo, considerando la struttura ad albero dello stesso. In
    particolare, ciascun nodo viene associato a una media dei vettori
    dei suoi figli, fino a giungere alla radice (l'intero articolo). Viene inoltre effettuata una media fra tutti i riferimenti per ottenere un solo vettore, tale vettore viene poi messo in media con il resto nel vettore del nodo corrente.

Oltre ai vettori rappresentanti ciascun articolo, viene anche
memorizzato un vettore rappresentante il titolo di ciascun documento.

Per quanto riguarda le ordinanze e le sentenze della Corte
Costituzionale, non sono presenti articoli e titoli, per cui la
procedura utilizzata per costruire le rappresentazioni vettoriali
diverge in modo significativo da quella usata per gli altri dataset:

1.  Vengono estratti tutti i dispositivi dai documenti (sono marcati con
    "decision" in Akoma Ntoso);

2.  Vengono estratti tutte le epigrafi dai documenti (marcate con
    "introduction" in Akoma Ntoso);

3.  Nel resto del testo non marcato come \"introduction\" o \"decision\"
    vengono estratti tutti i contenuti delle parentesi, che contengono
    la descrizione delle norme a cui il documento fa riferimento;

4.  Per creare un analogo dei vettori degli articoli, viene creata la
    rappresentazione vettoriale del dispositivo di ciascun documento, a
    cui si aggiunge un vettore ottenuto dalla media di dei sentence
    embedding di tutte le porzioni di testo fra parentesi discusse nel
    passaggio precedente;

5.  In sostituzione dei titoli, vengono usati i vettori dell'epigrafe di
    ciascun documento.

Per uniformare il linguaggio usato nelle sezioni precedenti nel contesto
dei documenti della Corte Costituzionale, usiamo "titolo" per riferirci
alle epigrafi, mentre con "articoli" intendiamo il vettore ottenuto dai
dispositivi e la media dei vettori del testo fra parentesi di cui al
punto 4.

## Classificazione

La classificazione avviene riconducendo ciascun documento alla
descrizione di una delle commissioni della camera. Per effettuare la
classificazione dei documenti, abbiamo sfruttato due addendi:

-   La similarità coseno fra gli embedding dei titoli e gli embedding
    delle descrizioni di ciascuna categoria (commissione o EuroVoc);

-   La media delle similarità degli articoli di ciascun documento (un
    valor medio per documento) e i vettori ottenuti dalle descrizioni di
    ciascuna categoria.

A questo punto, la somma di questi due valori di similarità viene usata
per determinare quale sia la categoria con la similarità più alta al
documento, effettuando in questo modo la classificazione.

## Query

Per ricercare documenti rilevanti, usiamo un approccio analogo a quello
utilizzato per la classificazione. In particolare, vengono usati due
elementi per la query:

-   Titolo: una query arbitraria indicata dall'utente, che può
    corrispondere al titolo di un documento;

-   Keywords: una lista di parole chiave rilevanti.

Come primo passaggio, viene creata una rappresentazione vettoriale delle
keywords, che vengono unite da \";\". Successivamente, viene calcolata
la rappresentazione vettoriale del titolo. Infine, viene fatta una somma
fra il vettore che rappresenta le keywords e un vettore che rappresenta
il titolo, ottenendo un vettore che rappresenta sia keyword che titolo
in egual misura (query embedding).

Il primo passaggio della query è quindi quello di individuare, tramite
similarità coseno fra il query embedding e gli embedding di ciascuna
descrizione delle categorie, le due categorie più appropriate per la
query. Successivamente viene calcolata la similarità fra il query
embedding e i documenti appartenenti alle due categorie individuate in
precedenza. In particolare, vengono sommate le similarità fra la query e
i titoli e la media fra le similarità della query con gli embedding
degli articoli di ciascun documento. Infine, viene fornita una lista dei
10 documenti che risultano più simili alla query e che sono appartenenti
a ciascuna delle due classe più appropriate.

## REST API

Lo strumento è accessibile tramite una REST API che può essere
interrogata tramite richieste POST. E' possibile specificare parametri
quali \"dataset\", che consente all'utente di specificare la base
documentale che vuole interrogare, \"title\" per la query arbitraria,
\"keywords\" per le parole chiave ed infine \"date\" che permette di
specificare una data opzionale per filtrare i risultati a documenti
precedenti alla data indicata. La richiesta restituisce una lista
all'interno della quale sono presenti le due categorie più simili
trovate, descritte in documenti JSON contenenti ciascuno il nome della
categoria, una lista di FRBRexpression simili alla query e una lista di
titoli simili.