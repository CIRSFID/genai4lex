from flask import Flask, request

import torch
import json
from sentence_transformers import SentenceTransformer
from sentence_transformers.util import cos_sim


app = Flask(__name__)
model = None


def get_most_similar_class(embedding, dataset, num_classes=2):
    data_dir = f"./data/{dataset}"
    class_embeddings = torch.load(
        f"{data_dir}/clusters_embeddings.pt", map_location="cpu"
    )

    similarities = cos_sim(embedding.view(1, -1), class_embeddings)

    top_similar = torch.topk(similarities[0], k=num_classes).indices

    with open(f"{data_dir}/clusters_names.txt") as f:
        clusters_names = f.read().splitlines()

    results_list = [
        (top_similar[idx], clusters_names[top_similar[idx]])
        for idx in range(num_classes)
    ]
    return results_list


def get_most_similar_document(query_embedding, dataset, class_idx, date):
    cluster_dir = f"./data/{dataset}/cluster{class_idx}"
    article_embeddings = torch.load(
        f"{cluster_dir}/article_embeddings.pt", map_location="cpu"
    )

    _, embedding_size = article_embeddings.size()

    doc_titles = []
    with open(f"{cluster_dir}/titles.txt") as f:
        for line in f:
            line = line[:-1]
            doc_titles.append(line)

    num_documents = len(doc_titles)

    with open(f"{cluster_dir}/article_spans.json") as f:
        document_spans = json.load(f)

    with open(f"{cluster_dir}/doc_dates.txt") as f:
        document_dates = f.read().splitlines()

    with open(f"{cluster_dir}/frbrexpressions.txt") as f:
        frbrexpressions = f.read().splitlines()

    doc_embeddings = torch.zeros(num_documents, embedding_size)
    excluded_mask = torch.ones(num_documents, dtype=torch.bool)
    for idx, span in enumerate(document_spans):
        if date is not None and document_dates[idx] > date:
            excluded_mask[idx] = 0
            continue

        doc_embeddings[idx] = torch.mean(
            article_embeddings[span[0] : span[1]], dim=0
        )

    title_embeddings = torch.load(
        f"{cluster_dir}/title_embeddings.pt", map_location="cpu"
    )
    doc_embeddings += title_embeddings

    doc_embeddings[~excluded_mask] = 0

    similarity = cos_sim(query_embedding.view(1, -1), doc_embeddings)[0]

    topk_output = torch.topk(similarity, k=min(10, len(similarity)))

    most_similar_values = topk_output.values
    most_similar_values_indices = torch.argwhere(most_similar_values)

    most_similar_indices = topk_output.indices
    most_similar_indices = [
        most_similar_indices[idx] for idx in most_similar_values_indices
    ]

    most_similar_titles = [doc_titles[idx] for idx in most_similar_indices]
    most_similar_frbrexpressions = [
        frbrexpressions[idx] for idx in most_similar_indices
    ]

    return (
        most_similar_titles,
        most_similar_frbrexpressions,
    )


def semantic_similarity(dataset, title, keywords, date):
    model = SentenceTransformer("paraphrase-multilingual-mpnet-base-v2")

    query_str_list = [title]
    if len(keywords) > 0:
        keywords = "; ".join(keywords)
        query_str_list.append(keywords)

    query_embedding = torch.mean(
        model.encode(
            query_str_list,
            convert_to_numpy=False,
            convert_to_tensor=True,
            device="cpu",
        ),
        dim=0,
    )

    class_results_list = get_most_similar_class(query_embedding, dataset)

    results = []
    for class_idx, class_name in class_results_list:
        curr_result = {}
        (
            most_similar_titles,
            most_similar_frbrexpressions,
        ) = get_most_similar_document(
            query_embedding, dataset, class_idx, date
        )

        curr_result["class_name"] = class_name
        curr_result["titles"] = most_similar_titles
        curr_result["frbrexpressions"] = most_similar_frbrexpressions

        results.append(curr_result)

    return results


@app.route("/", methods=["POST"])
def api_wrapper():
    params = request.get_json()
    dataset = params["dataset"]
    if dataset not in [
        "leos",
        "normattiva",
        "disegnidilegge",
        "cortecostituzionale",
    ]:
        return
    title = params["title"]
    keywords = params["keywords"]
    if "date" not in params or params["date"] == "":
        date = None
    else:
        date = params["date"]

    return semantic_similarity(dataset, title, keywords, date)


def load_model():
    global model
    model = SentenceTransformer("paraphrase-multilingual-mpnet-base-v2")
    return


if __name__ == "__main__":
    load_model()
    app.run(host="0.0.0.0", port=9642)
