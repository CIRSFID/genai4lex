# Istruzioni per l'uso

Serve un account gmail per poter accedere alla [demo](https://colab.research.google.com/drive/1dLlg2-jeeegcSJiZcLP2qO41tpS04puc?usp=sharing#scrollTo=Jv6Q95teQeep).


# Trascrittore Interattivo

Sistema di trascrizione automatica e ricerca che permetta un accesso semplificato al lavoro delle commissioni parlamentari.

Il sistema permette all’utente di svolgere diverse azioni:

1. ottenere una trascrizione scritta di una seduta a partire dalla registrazione video;
2. esplorare tramite interrogazioni a testo libero il contenuto delle trascrizioni. Sono ammesse ad esempio delle query del tipo “Qual è il parere di **X** sul tema **Y**?”
3. combinare le sedute in una sola base di conoscenza in modo da permettere delle interrogazioni su temi ricorrenti.

## Pipeline di Elaborazione

![schema](https://gitlab.com/CIRSFID/genai4lex/-/raw/develop/transcription/UNIBO/transcriptor/imgs/parlamento.png)

La pipeline implementata segue questi step principali:

1. Estrazione della traccia audio dal video
2. Trascrizione e diarizzazione
3. Identificazione degli speaker mediante LLM
4. Segmentazione del testo, embedding e popolamento del database vettoriale
5. Ricerca e generazione output mediante LLM

### Estrazione della traccia audio dal video

La prima fase del processo consiste nell'isolamento della traccia audio dai video delle sedute. Questo passaggio è essenziale per garantire che la qualità del suono sia sufficiente per una trascrizione accurata.

### Trascrizione e Diarizzazione

In questa fase, la trascrizione del testo parlato viene effettuata utilizzando il modello Whisper v3, un Large Language Model (LLM) specializzato nella trascrizione automatica. Whisper v3 è noto per la sua capacità di convertire con precisione l'audio in testo scritto, indipendentemente dalla lingua o dall'accento degli speaker. Questa tecnologia è particolarmente utile in contesti multilingue e in situazioni in cui la qualità dell'audio può variare.

Il prototipo integra delle funzionalità di diarizzazione, un processo che consente di distinguere e classificare i diversi speaker presenti all'interno della traccia audio. La diarizzazione è fondamentale per ottenere una trascrizione accurata, specialmente in contesti dove diversi oratori possono intervenire in rapida successione. Separare i contributi dei diversi speaker aiuta a migliorare la precisione della trascrizione, riducendo la confusione tra le voci e assicurando che ogni intervento sia attribuito correttamente. In genere, si utilizzano tecniche di riconoscimento del parlato per segmentare l'audio, identificando quando un nuovo speaker inizia a parlare e assegnando etichette temporanee a ciascun segmento.

### Identificazione degli speaker mediante LLM

Nel contesto delle sedute di commissione, è comune osservare una struttura ricorrente in cui il presidente della commissione introduce e cede la parola ai relatori. Questa struttura offre un'opportunità per l'identificazione dei relatori. Nel nostro caso, facciamo uso del modello Llama3:70b per estrarre l’identità dell’ignoto a partire dall’intervento che lo precede. Questo il prompt utilizzato:

```
{Intervento del relatore}

A chi viene ceduta la parola secondo il testo che ti è stato dato?
Se non è possibile estrarre l'informazione restituisci NA.
Negli altri casi, l'output deve avere questa struttura: Nome Cognome
```

Grazie all'identificazione dei relatori, è possibile eseguire query mirate su argomenti specifici, come ad esempio chiedere l'opinione di un relatore particolare su un determinato tema. Questo aumenta la precisione e la pertinenza dei risultati restituiti dal sistema di ricerca, consentendo di estrarre informazioni più precise e pertinenti.


#### Segmentazione del testo, embedding e popolamento del database vettoriale

Una volta ottenuta la trascrizione del testo della seduta parlamentare, la prima operazione consiste nella segmentazione del testo in unità più piccole, che possono essere trattate individualmente. Questo passaggio è cruciale per poter effettuare analisi più dettagliate e per rendere più efficienti le operazioni successive.

Il testo viene suddiviso nei singoli interventi, e ogni intervento viene ulteriormente segmentato in frammenti di massimo 400 caratteri. Ogni frammento di testo è integrato con l'identità di chi ha pronunciato quella frase, in modo che l'informazione sull'oratore sia facilmente associabile e utilizzabile in fase di ricerca. 

Dopo la segmentazione del testo, ogni frammento viene convertito in un embedding vettoriale. Gli embeddings sono rappresentazioni numeriche dei testi che catturano le caratteristiche semantiche e sintattiche del testo stesso. Questo passaggio è essenziale per poter calcolare la similarità tra i frammenti di testo e le query degli utenti in modo efficiente. Nel contesto di questo sistema, la similarità tra due vettori in uno spazio vettoriale è calcolata tramite la similarità coseno. Più la similarità coseno è alta, più i due frammenti di testo sono simili. Questo concetto è fondamentale per il processo di recupero delle informazioni, in quanto consente di identificare i frammenti di testo più pertinenti rispetto alla query dell'utente.

Gli embeddings dei frammenti di testo vengono quindi utilizzati per popolare un database vettoriale. Questo database contiene gli embeddings di tutti i frammenti di testo estratti dalla sedute. L'obiettivo principale di questo database è fornire un sistema di recupero efficiente per trovare i frammenti di testo più rilevanti rispetto alle query degli utenti.

#### Ricerca e generazione output mediante LLM

L’embedding del testo è fondamentale negli approcci RAG (“Retrieve and Generate”) alla generazione di testo mediante LLM. Questi approcci combinano due fasi principali: il recupero di informazioni rilevanti e la generazione di una risposta basata su tali informazioni.

1. **Recupero**: Nella fase di recupero, viene effettuata una ricerca nel corpus di testo o nei dati disponibili per trovare le informazioni rilevanti rispetto alla domanda dell'utente. Questa ricerca può essere basata su parole chiave, concetti o altri metodi di ricerca per identificare i frammenti di testo più pertinenti.

2. **Generazione**: Una volta recuperati i frammenti di testo rilevanti, nella fase di generazione viene utilizzato un modello di linguaggio per generare una risposta coerente e informativa alla domanda dell'utente.

Gli approcci di RAG sono ampiamente utilizzati in diversi contesti, tra cui assistenti virtuali, motori di ricerca e sistemi di risposta automatica. Questi approcci consentono di combinare i vantaggi del recupero di informazioni specifiche con la capacità di generare testo in modo contestuale e coerente, fornendo risposte accurate e informative agli utenti.

Il nostro sistema implementa un approccio di tipo "Retrieve and Generate" (RAG) per generare le risposte alle query degli utenti. Quando viene ricevuta una query, il sistema utilizza la similarità coseno tra l'embedding della query e gli embeddings dei frammenti di seduta presenti nel database vettoriale per reperire i frammenti più rilevanti. Successivamente, questi frammenti vengono integrati in un prompt generativo, che viene utilizzato per generare una risposta coerente e informativa alla query dell'utente. La scelta di frammenti di 400 caratteri è pensata per massimizzare la varietà di informazioni in input nel caso di un con contesto relativamente piccolo come quello di llama3 (8k token). 

Segue un **esempio di interazione** con il sistema, data la query

```
Qual è il contributo di Giulia Zanotelli al lavoro della commissione?
```

e una base di conoscenza costruita a partire dalla seduta del [24 Aprile 2024 (Commissione Ambiente)](https://webtv.camera.it/evento/25198). Il modello utilizzato è llama3:70b.
 
Nella prima fase vengono recuperati i 10 frammenti di seduta più rilevanti – cioè quelli con similarità coseno più alta – rispetto alla query fornita. 

```
10_0_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8917126716223065
Il relatore Giulia Zanotelli dichiara:
Grazie mille, grazie Presidente, grazie ai parlamentari, grazie per averci dato questa opportunità. Abbiamo inviato come Commissione anche una nota che riepiloga un po' i contenuti rispetto al disegno di legge quadro in materia di ricostruzione post calamità.  e quindi da parte mia riporterò quanto immerso all'interno delle discussioni politiche. Premetto che già abbiamo avuto occasione di


10_3_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8826512043896986
Il relatore Giulia Zanotelli dichiara:
evidenziano alcuni passaggi contenuti all'interno del disegno di legge che pongono alcune questioni secondo noi di attenzione da parte della Commissione ma anche da parte del Governo e in particolar modo vanno a toccare quella che secondo noi è un'incoerenza tra le finalità della norma  e l'applicabilità della stessa all'atto pratico emerge un tema collegato alla semplificazione quindi lo stesso


10_10_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8773191393358319
Il relatore Giulia Zanotelli dichiara:
amministrazioni e i territori,  e soprattutto anche un'assenza di appetibilità di questi materiali nelle dinamiche di riuso produttivo. L'ultimo tema che noi abbiamo sollevato è quello legato alle assicurazioni sicuramente con questo disegno di legge si è fatto un passo in avanti ma noi riteniamo che qualcosa in più si possa fare  ad esempio andando a incidere su quella che è la detrazione dei


10_8_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8735121875090124
Il relatore Giulia Zanotelli dichiara:
normativa ma che soprattutto nei casi eccezionali collegati a questi tipi di eventi pone molte difficoltà soprattutto quando si parla di rifiuti e di terre e rocce da scavo che necessitano di interventi normativi  appunto come dicevano che hanno bisogno sicuramente di una rivisitazione. Si riferisce in particolar modo a questo disegno di legge soprattutto per quanto riguarda i materiali


10_2_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8731554272956504
Il relatore Giulia Zanotelli dichiara:
3,1, laddove si individua quale possibile figura commissariale, quella del Presidente della Regione,  noi riteniamo che questa debba essere necessariamente inserita come norma. Questo perché si ritiene che qualsiasi scelta che riguarda i territori debba essere condivisa con gli stessi ponendo la centralità agli enti territoriali che vengono interessati  dal tema della ricostruzione. Inoltre si


10_6_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8716007920067439
Il relatore Giulia Zanotelli dichiara:
disposizioni di potenziamento delle strutture in questo disegno di legge sono concesse e sono in capo al Dipartimento  Inoltre appunto collegandoci sempre al passaggio che ho fatto prima legato anche alla semplificazione e quindi anche ad un'accelerazione che serve nelle procedure diciamo che il disegno di legge  rispetto a molte parti in realtà rimanda in via anche altri tipi di normative, di


10_1_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8666259706083261
Il relatore Giulia Zanotelli dichiara:
incontrare il Ministro Musumeci su questo tema, avendone discusso in più occasioni. Esprimiamo positività rispetto ad alcune proposte emendative che sono state accolte, mentre altre,  ad oggi, e poi andrò ad illustrare i dettagli, non hanno ottenuto attualmente un via libera. Per quanto riguarda il disegno di legge 1632, come vi dicevo, pur apprezzando la modifica che è stata portata all'articolo


10_4_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8661887605995842
Il relatore Giulia Zanotelli dichiara:
provvedimento che cerca di porsi come codice unico secondo noi riscontra poi nelle realtà criticità  per la diversità degli eventi nei quali questo deve trovare applicazione eventi che come potete capire sono molto diversi fra di loro e quindi hanno bisogno anche di procedure diverse inoltre si evidenzia un tema collegato alle tempistiche quindi tempistiche molto lunghe e collegate a provvedimenti


10_11_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8661794185985794
Il relatore Giulia Zanotelli dichiara:
premi assicurativi versati da parte dei contribuenti. Per quanto riguarda invece gli ulteriori due disegni di legge, quindi il 589 e il 647, la Commissione già negli scorsi mesi ha espresso un parere negativo  In particolar modo sul disegno di legge 589 gli assessori hanno evidenziato come in questo disegno di legge venga meno la centralità  dei territori e quindi ci sia un ruolo delle regioni


10_7_commissione_ambiente_2024_04_24_w_Giulia_Zanotelli.txt - 0.8631878938320818
Il relatore Giulia Zanotelli dichiara:
decreti e di direttive e quindi questo sicuramente non facilita diciamo così quello che è l'iter. Un altro tema molto rilevante che è emerso all'interno dei lavori della Commissione Protezione Civile e che si ricollega al tema della ricostruzione è il tema collegato alla gestione dei rifiuti, una materia che secondo noi già  nella ordinarietà avrebbe bisogno di una modifica, di una revisione
```

Questi frammenti vengono quindi uniti alla query in input e forniti al modello linguistico.
Questa la risposta generata:

```
Giulia Zanotelli, come relatore, ha espresso la sua opinione sulla proposta di legge quadro in materia di ricostruzione post-calamità. Ha evidenziato alcune criticità e incoerenze all'interno del disegno di legge, tra cui la semplificazione delle procedure, l'assenza di appetibilità dei materiali nelle dinamiche di riuso produttivo e la necessità di una rivisitazione della normativa sui rifiuti e terre e rocce da scavo. Ha anche espresso la sua preoccupazione per le tempistiche molto lunghe e collegate a provvedimenti normativi. Inoltre, ha sottolineato l'importanza della centralità degli enti territoriali nella gestione dei territori interessati dalla ricostruzione.
```

## Strumenti

Segue la lista di modelli linguistici e strumenti di supporto utilizzati per l'implementazione del prototipo.

### Modelli Linguistici

#### [openai/whisper-large-v3](https://huggingface.co/openai/whisper-large-v3)
   
Whisper Large v3 è un modello open-source di riconoscimento vocale sviluppato da OpenAI. È progettato per trascrivere il linguaggio parlato in testo con alta precisione, supportando più lingue e dialetti.

#### [meta-llama/Meta-Llama-3-70B](https://huggingface.co/meta-llama/Meta-Llama-3-70B)
   
Meta LLaMA 3 70B è un modello linguistico open-source sviluppato da Meta. Con 70 miliardi di parametri, è progettato per produrre testo coerente e contestualmente rilevante in una vasta gamma di applicazioni. Questo modello eccelle in compiti come scrittura creativa, creazione automatizzata di contenuti e generazione di risposte conversazionali, rendendolo uno strumento potente per compiti di elaborazione del linguaggio naturale.

#### [intfloat/multilingual-e5-large](https://huggingface.co/intfloat/multilingual-e5-large)
   
Multilingual E5 Large è un modello di embeddings creato da intfloat che si specializza nella generazione di rappresentazioni vettoriali di alta qualità del testo in più lingue. Questi embeddings sono utili per una varietà di compiti NLP, tra cui ricerca semantica, classificazione del testo e clustering. La capacità del modello di gestire dati multilingue lo rende una scelta eccellente per applicazioni che richiedono comprensione e elaborazione interlinguistica.


### Strumenti di supporto

#### [ollama](https://github.com/ollama/ollama)
**Ollama** è un framework open-source progettato per facilitare agli sviluppatori il deployment e la gestione dei modelli di linguaggio. Il framework si concentra nel facilitare l'integrazione e il deployment dei modelli NLP in varie applicazioni, fornendo strumenti e librerie che semplificano compiti come l'addestramento, l'affinamento e la messa in servizio dei modelli.

#### [whisperX-FastAPI](https://github.com/pavelzbornik/whisperX-FastAPI)
**WhisperX-FastAPI** fornisce un'interfaccia API per [whisperX](https://github.com/m-bain/whisperX), libreria per la gestione dei modelli della famiglia Whisper, facilitando l'integrazione del servizio di trascrizione e diarizzazione all'interno del sistema.

#### [cheshire-cat-ai/core](https://github.com/cheshire-cat-ai/core)
**Cheshire Cat** è un framework open-source progettato per lo sviluppo di agenti AI intelligenti basati su modelli di linguaggio di grandi dimensioni (LLM). Il framework è agnostico rispetto ai modelli di linguaggio, e permette di gestire in modo del tutto trasparente tutte le operazioni legate al RAG (generazione e interrogazione di un database vettoriale, recupero dei frammenti di testo pertinenti rispetto alle query degli utenti, generazione output).


## Interfaccia Utente

L’interfaccia è disponibile tramite [notebook Jupyter](https://gitlab.com/CIRSFID/genai4lex/-/blob/develop/transcription/UNIBO/transcriptor/demo.ipynb) e presenta due funzionalità principali:

1. **Generazione della trascrizione**: Richiede in input il link del video da trascrivere (Video) e l’identificativo del database da utilizzare per il salvataggio del testo della trascrizione (Contesto). Restituisce il testo della trascrizione.

![schermata 1](https://gitlab.com/CIRSFID/genai4lex/-/raw/develop/transcription/UNIBO/transcriptor/imgs/f1.png)

2. **Interrogazione della trascrizione**: richiede in input la query per il sistema (Domanda) e l’identificatico del database da utilizzare per il popolamento del contesto (Contesto). Restituisce la risposta del sistema e la lista di frammenti utilizzati.

![schermata 2](https://gitlab.com/CIRSFID/genai4lex/-/raw/develop/transcription/UNIBO/transcriptor/imgs/f2.png)

Prima dell’utilizzo, il notebook deve essere configurato in modo da includere i riferimenti alle istanze di [whisperX-FastAPI](https://github.com/pavelzbornik/whisperX-FastAPI) e [cheshire-cat-ai/core](https://github.com/cheshire-cat-ai/core). Per il loro setup si rimanda alle rispettive documentazioni.

**Cachire-cat** è stato utilizzato con la seguente configurazione:

- **Plugins**: *Rabbithole Segmentation*, *Cheshire Cat Prompt Settings*
- **LLM**: *llama3:70b* (temperature: 0.0)
- **Embedder**: *intfloat/multilingual-e5-large* (max-lenght: 512)
- **Impostazioni Prompt**: *Memoria episodica disattivata*, *# memorie dichiarative: 10*, *soglia memorie dichiarative: 0.4*  
