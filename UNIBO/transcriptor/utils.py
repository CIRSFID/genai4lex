import time
import json
import cheshire_cat_api as ccat


class LLMManager:

  _instance = None    
  
  content = ""

  def __new__(cls, *args, **kwargs):
      if cls._instance is None:
          cls._instance = super(LLMManager, cls).__new__(cls)
      return cls._instance
    
  def __init__(self, url, port):
    self._url = url
    self._port = port

  def on_open():
      # This is triggered when the connection is opened
      # print("Connection opened!")
      pass

  def on_message(message: str):
      # This is triggered when a new message arrives
      # and grabs the message
      # print(message)

      response = json.loads(message)
      if response["type"] != "chat_token":
        # print(response)
        pass
      if response["type"] == "chat":
        LLMManager.content = response

  def on_error(exception: Exception):
      # This is triggered when a WebSocket error is raised
      print(str(exception))
      LLMManager.content = str(exception)

  def on_close(status_code: int, message: str):
      # This is triggered when the connection is closed
      # print(f"Connection closed!")
      pass


  def connect(self, user_id):
    # Connection settings with default values
    config = ccat.Config(
        base_url=self._url,
        port=self._port,
        user_id=user_id,
        auth_key="",
        secure_connection=False
    )

    # Cat Client
    cat_client = ccat.CatClient(
        config=config,
        on_open=LLMManager.on_open,
        on_close=LLMManager.on_close,
        on_message=LLMManager.on_message,
        on_error=LLMManager.on_error
    )

    # Connect to the WebSocket API
    cat_client.connect_ws()

    while not cat_client.is_ws_connected:
        time.sleep(1)

    return cat_client

  def disconnect(self, cat_client):
    cat_client.close()


  def ask_gpt(self, cat_client, prompt):
    LLMManager.content = ""
    cat_client.send(message=prompt)

    while LLMManager.content == "":
      True

    return LLMManager.content


  def prompt(self, user_id, prompt):
    client = self.connect(user_id)
    res = self.ask_gpt(client, prompt)
    self.disconnect(client)
    return res["content"], [( x["metadata"]["source"], x["score"], x["page_content"]) for x in res["why"]["memory"]["declarative"]]


  def clean_history(self, user):
    import requests

    url = f"http://{self._url}:{self._port}/memory/conversation_history/"

    payload = {}
    headers = {
      'Accept': 'application/json',
      'user_id': user
    }

    response = requests.request("DELETE", url, headers=headers, data=payload)
    #print(response.text)

  def get_history(self, user):
    import requests

    url = f"http://{self._url}:{self._port}/memory/conversation_history/"

    payload = {}
    headers = {
      'Accept': 'application/json',
      'user_id': user
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    print(response.text)

  def upload_memory(self, user, chunk_size, file_name, file_path):
    import http.client
    import mimetypes
    from codecs import encode
    import time

    conn = http.client.HTTPConnection(self._url, self._port)
    dataList = []
    boundary = 'WebKitFormBoundary7MA4YWxkTrZu0gW'
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=file; filename={0}'.format(file_name)))

    fileType = mimetypes.guess_type(file_path)[0] or 'application/octet-stream'
    dataList.append(encode('Content-Type: {}'.format(fileType)))
    dataList.append(encode(''))

    with open(file_path, 'rb') as f:
      dataList.append(f.read())
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=chunk_size;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode(str(chunk_size)))
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=chunk_overlap;'))

    dataList.append(encode('Content-Type: {}'.format('text/plain')))
    dataList.append(encode(''))

    dataList.append(encode("100"))
    dataList.append(encode('--'+boundary+'--'))
    dataList.append(encode(''))
    body = b'\r\n'.join(dataList)
    payload = body
    headers = {
      'Content-type': 'multipart/form-data; boundary={}'.format(boundary),
      'Accept': 'application/json',
      'user_id': user
    }
    conn.request("POST", "/rabbithole/", payload, headers)
    res = conn.getresponse()
    data = res.read()
    # print(data.decode("utf-8"))

  def upload_all_memories_folder(self, user, chunk_size, folder):
    import os
    files = os.listdir(folder)

    for file_name in files:
      if file_name.endswith(".txt"):
        file_path = os.path.join(folder, file_name)
        self.upload_memory(user, chunk_size, file_name, file_path)
        time.sleep(1)
        print(".", end='')


class TranscriptionManager:

  _instance = None    
  
  content = ""

  def __new__(cls, *args, **kwargs):
      if cls._instance is None:
          cls._instance = super(TranscriptionManager, cls).__new__(cls)
      return cls._instance
    
  def __init__(self, url, port, llm):
    self._url = url
    self._port = port
    self._llm = llm

  def start_transcription(self, target):
    import requests
    import json

    url = f"http://{self._url}:{self._port}/speech-to-text-url?language=it&task=transcribe&model=large-v3&device=cuda&device_index=0&threads=0&batch_size=16&compute_type=float16&align_model=facebook/wav2vec2-large-960h-lv60&interpolate_method=nearest&return_char_alignments=false&min_speakers=1&max_speakers=30&beam_size=5&patience=1&length_penalty=1&temperatures=0&compression_ratio_threshold=2.4&log_prob_threshold=-1&no_speech_threshold=0.6&initial_prompt=<string>&suppress_tokens=-1&suppress_numerals=false&vad_onset=0.5&vad_offset=0.363"

    from urllib.parse import quote
    encoded_target = quote(target)

    payload = f'url={encoded_target}'
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)
    return json.loads(response.text)["identifier"]


  def get_result(self, id):
    import requests
    import json

    url = f"http://{self._url}:{self._port}/task/{id}"

    payload = {}
    headers = {
      'Accept': 'application/json'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    # print(response.text)
    return json.loads(response.text)

  def all_tasks(self):
    import requests
    import json

    url = f"http://{self._url}:{self._port}/task/all"

    payload = {}
    headers = {
      'Accept': 'application/json'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    # print(response.text)
    return json.loads(response.text)

  def delete_task(self, id):
    import requests
    import json

    url = f"http://{self._url}:{self._port}/task/{id}/delete"

    payload = {}
    headers = {
      'Accept': 'application/json'
    }

    response = requests.request("DELETE", url, headers=headers, data=payload)

    # print(response.text)
    return json.loads(response.text)

  def _wait_results(self, id):

    import time

    result = { "status" : ""}
    while result["status"] not in ["completed", "failed"]:
      result = self.get_result(id)
      print(".", end='')
      time.sleep(3)
    if result["status"] == "failed": raise Exception("Error generating transcription")

    res = []

    for x in result["result"]["segments"]:
      if len(res) == 0 or res[-1]["speaker"] != x["speaker"]:
        res.append({
            "speaker": x["speaker"],
            "text": [x["text"]]
        })
      else:
        res[-1]["text"].append(x["text"])

    res_flatted = [{x["speaker"] : " ".join(x["text"])} for x in res]
    return res_flatted

  def _generate_speakers_set(self, result):

    def get_speaker(x):
      return list(x.keys())[0]

    def get_text(x):
      return list(x.values())[0]

    transcription = result.copy()

    replace_set = { list(transcription[0].keys())[0] : "Presidente" }

    for idx, x in enumerate(transcription):
      if get_speaker(x).startswith("SPEAKER") and get_speaker(x) not in replace_set:
        n = get_text(transcription[idx - 1])
        self._llm.clean_history("extract_speaker")
        speaker = self._llm.prompt("extract_speaker", n + "\n\n\nA chi viene ceduta la parola secondo il testo che ti è stato dato? Se non è possibile estrarre l'informazione restituisci NA. Negli altri casi, l'output deve avere questa struttura: Nome Cognome")
        replace_set[get_speaker(x)] = speaker[0]
        print(".", end='')

    return replace_set

  def _final_transcription(self, transcription, replace_set):

    def replace_speaker(trans, speaker, target):
      return [{speaker if x == target else x : y} for z in trans for x, y in z.items()]

    t_1 = transcription.copy()

    for x, y in replace_set.items():
      t_1 = replace_speaker(t_1, y, x)

    return t_1

  def _save_in_folder(self, name, transcription):

    def save_result(res, file_path):
      with open(file_path, 'w', encoding='utf-8') as file:
        file.write(res)
        
    import os
    os.mkdir(name)
    for idx, x in enumerate(transcription):
      for s, t in x.items():
        import textwrap
        lines = textwrap.wrap(t, 400, break_long_words=False)
        for idx_2, l in enumerate(lines):
          save_result(f"Il relatore {s} dichiara:\n {l}", f"./{name}/{idx}_{idx_2}_{name}_{s.replace(' ', '_')}.txt")

  def compute(self, url, context):
    print("Transcribing")
    id = self.start_transcription(url)
    result = self._wait_results(id)
    print("\nGenerating speakers")
    replace_set = self._generate_speakers_set(result)
    transcription = self._final_transcription(result, replace_set)
    print("\nIngesting")
    self._save_in_folder("temp", transcription)
    self._llm.upload_all_memories_folder(context, 500, "temp")
    return transcription
