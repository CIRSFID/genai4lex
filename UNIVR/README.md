**PRESENTAZIONE DI HOUDINI**

Sul sito GitLab su cui state navigando sono presenti

- La norma tradotta con a fronte il formato machine-readable;
- Il testo della traduzione in formato analogo a quello del sistema Houdini;
- La norma in originale (proposta di legge 100/2018)

Viceversa Houdini online è accessibile da

<http://www.houdini-ddl.it:8081/> 

Infine, la presentazione in formato video di Houdini è disponibile

[https://univr-my.sharepoint.com/:v:/g/personal/matteo_cristani_univr_it/ETeHuK03i7tBuCyIsSMrnsEBijCexcgvwlCVbpmsknfiCg ](https://univr-my.sharepoint.com/:v:/g/personal/matteo_cristani_univr_it/ETeHuK03i7tBuCyIsSMrnsEBijCexcgvwlCVbpmsknfiCg) 


